// This test compiles a contest with `examples/` in this crate

use mapm::contest::fetch_contest;
use mapm::result::MapmErr::*;

#[test]
fn compile() {
    let templates_dir = "examples/templates";
    let contest = fetch_contest(
        "examples/contests/minimal-template-example.yml",
        "examples/problems/",
        templates_dir,
    )
    .unwrap();
    let contest_msgs = contest.compile(templates_dir);
    if let Some(err) = contest_msgs.err {
        match err {
            ProblemErr(err) => {
                panic!("Problem error: {}", err);
            }
            ContestErr(err) => {
                panic!("Contest error: {}", err);
            }
            TemplateErr(err) => {
                panic!("Template error: {}", err);
            }
            SolutionErr(err) => {
                panic!("Solution error: {}", err);
            }
        }
    }
    for result in contest_msgs.latexmk_results {
        match result {
            Ok(msg) => println!("{}", msg),
            Err(err) => panic!("{}", err),
        }
    }
}
