//! Fetching and filtering functions for problems

use crate::result::MapmErr;
use crate::result::MapmErr::*;
use crate::result::MapmResult;

use crate::template::Template;
use Filter::*;
use FilterAction::*;
use Views::*;

use std::fs;
use std::path::Path;

use std::collections::HashMap;

use serde::{Deserialize, Serialize};

use serde_yaml::Value;

#[derive(Debug, Serialize, Deserialize)]
struct SerializedProblem {
    pub solutions: Option<Solutions>,
    #[serde(flatten)]
    pub vars: HashMap<String, Value>,
}

pub type Vars = HashMap<String, String>;
pub type Solutions = Vec<HashMap<String, String>>;

#[derive(Debug, PartialEq, Clone)]
pub struct Problem {
    pub name: String,
    pub vars: Vars,
    pub solutions: Solutions,
}

/// Defines types of filters for problems
///
/// Only the "Exists" variant may be used for the special key "solutions".
///
/// For the Gt, Lt, Ge, Le conditions, whoever is working with the problem files (either through CLI utils or a GUI) is responsible for ensuring that any key being compared with Gt, Lt, Ge, Le is a non-negative integer

#[derive(Clone)]
pub enum Filter {
    Exists { key: String },
    Eq { key: String, val: String },
    Gt { key: String, val: u32 },
    Lt { key: String, val: u32 },
    Ge { key: String, val: u32 },
    Le { key: String, val: u32 },
}

/// Defines the actions for filtering problems
///
/// Positive means "keep if this condition is satisfied", negative means "keep if this condition is **not** satisfied"
#[derive(Clone)]
pub enum FilterAction {
    Positive(Filter),
    Negative(Filter),
}

/// Views *only* shown strings or views everything *but* hidden strings
#[derive(Clone)]
pub enum Views {
    Show(Vec<String>),
    Hide(Vec<String>),
}

/// Fetches problem given the name (which corresponds to the filename) and the directory to find it
/// in

pub fn fetch_problem<T: AsRef<Path>>(problem_name: &str, problem_dir: T) -> MapmResult<Problem> {
    let problem_dir: &Path = problem_dir.as_ref();
    let problem_path = &problem_dir.join(&format!("{}.yml", problem_name));
    match fs::read_to_string(problem_path) {
        Ok(problem_yaml) => parse_problem_yaml(problem_name, &problem_yaml),
        Err(_) => Err(ProblemErr(format!(
            "Could not read problem `{}` from {:?}",
            problem_name, problem_path
        ))),
    }
}

impl Problem {
    /// Checks if a problem passes or fails a certain filter
    ///
    /// For Gt, it checks if the value of the problem is greater than the value passed in the filter (not the other way around). The same is true of Lt, Ge, Le.
    ///
    /// # Usage
    ///
    /// ```
    /// use mapm::problem::Problem;
    /// use mapm::problem::Filter;
    /// use std::collections::HashMap;
    ///
    /// let mut vars: HashMap<String, String> = HashMap::new();
    /// vars.insert(String::from("problem"), String::from("What is $1+1$?"));
    /// vars.insert(String::from("author"), String::from("Dennis Chen"));
    /// vars.insert(String::from("difficulty"), String::from("5"));
    ///
    /// let mut solution_one: HashMap<String, String> = HashMap::new();
    /// solution_one.insert(String::from("text"), String::from("It's probably $2$."));
    /// solution_one.insert(String::from("author"), String::from("Dennis Chen"));
    /// let mut solution_two = HashMap::new();
    /// solution_two.insert(String::from("text"), String::from("The answer is $2$, but my proof is too small to fit into the margin."));
    /// solution_two.insert(String::from("author"), String::from("Pierre de Fermat"));
    ///
    /// let mut solutions: Vec<HashMap<String, String>> = vec![solution_one, solution_two];
    ///
    /// let problem = Problem { name: String::from("problem"), vars, solutions };
    ///
    /// assert_eq!(problem.try_filter(&Filter::Exists{key: String::from("subject")}), false);
    /// assert_eq!(problem.try_filter(&Filter::Exists{key: String::from("solutions")}), true);
    ///
    /// assert_eq!(problem.try_filter(&Filter::Gt{key: String::from("difficulty"), val: 5}), false);
    /// assert_eq!(problem.try_filter(&Filter::Ge{key: String::from("difficulty"), val: 5}), true);
    /// assert_eq!(problem.try_filter(&Filter::Lt{key: String::from("difficulty"), val: 5}), false);
    /// assert_eq!(problem.try_filter(&Filter::Le{key: String::from("difficulty"), val: 5}), true);
    ///
    /// ```

    pub fn try_filter(&self, filter: &Filter) -> bool {
        match filter {
            Exists { key } => {
                if key == "solutions" {
                    !self.solutions.is_empty()
                } else {
                    self.vars.contains_key(key)
                }
            }
            Eq { key, val } => match self.vars.get(key) {
                Some(problem_value) => problem_value == val,
                None => false,
            },
            Gt { key, val } => match self.vars.get(key) {
                Some(problem_value) => match problem_value.parse::<u32>() {
                    Ok(problem_value_u32) => problem_value_u32 > *val,
                    Err(_) => false,
                },
                None => false,
            },
            Lt { key, val } => match self.vars.get(key) {
                Some(problem_value) => match problem_value.parse::<u32>() {
                    Ok(problem_value_u32) => problem_value_u32 < *val,
                    Err(_) => false,
                },
                None => false,
            },
            Ge { key, val } => match self.vars.get(key) {
                Some(problem_value) => match problem_value.parse::<u32>() {
                    Ok(problem_value_u32) => problem_value_u32 >= *val,
                    Err(_) => false,
                },
                None => false,
            },
            Le { key, val } => match self.vars.get(key) {
                Some(problem_value) => match problem_value.parse::<u32>() {
                    Ok(problem_value_u32) => problem_value_u32 <= *val,
                    Err(_) => false,
                },
                None => false,
            },
        }
    }

    /// Checks whether a problem satisfies a set of filters, and if it does, return it; otherwise return `None`
    ///
    /// If `filters` is empty, then every problem will pass the filter.

    pub fn filter(self, filters: &[FilterAction]) -> Option<Problem> {
        for filter_action in filters {
            match filter_action {
                Positive(filter) => {
                    if !self.try_filter(filter) {
                        return None;
                    }
                }
                Negative(filter) => {
                    if self.try_filter(filter) {
                        return None;
                    }
                }
            }
        }
        Some(self)
    }

    /// Show only the keys passed in or everything but the keys passed in to a problem
    ///
    /// # Usage
    ///
    /// ```
    /// use mapm::problem::Views;
    /// use mapm::problem::Views::{Show, Hide};
    /// use mapm::problem::Problem;
    /// use mapm::problem::Filter;
    /// use std::collections::HashMap;
    ///
    /// let problem = Problem {
    ///     name: String::from("problem"),
    ///     vars: HashMap::from([
    ///         (String::from("problem"), String::from("What is $1+1$?")),
    ///         (String::from("author"), String::from("Dennis Chen")),
    ///         (String::from("answer"), String::from("2"))
    ///     ]),
    ///     solutions: vec![HashMap::from([
    ///         (String::from("text"), String::from("It's $2$.")),
    ///         (String::from("author"), String::from("Alexander")),
    ///     ])]
    /// };
    ///
    /// let show: Views = Show(vec![String::from("author"), String::from("answer"), String::from("solutions")]);
    /// let show_filtered_problem = problem.clone().filter_keys(&show);
    /// assert_eq!(show_filtered_problem.0, HashMap::from([
    ///     (String::from("author"), String::from("Dennis Chen")),
    ///     (String::from("answer"), String::from("2"))
    /// ]));
    /// assert_eq!(show_filtered_problem.1, Some(vec![
    ///     HashMap::from([
    ///         (String::from("text"), String::from("It's $2$.")),
    ///         (String::from("author"), String::from("Alexander"))
    ///     ])
    /// ]));
    ///
    /// let hide: Views = Hide(vec![String::from("solutions"), String::from("author")]);
    /// let hide_filtered_problem = problem.clone().filter_keys(&hide);
    /// assert_eq!(hide_filtered_problem.0, HashMap::from([
    ///     (String::from("problem"), String::from("What is $1+1$?")),
    ///     (String::from("answer"), String::from("2")),
    /// ]));
    /// assert_eq!(hide_filtered_problem.1, None);
    /// ```

    pub fn filter_keys(self, views: &Views) -> (Vars, Option<Solutions>) {
        match views {
            Show(tags) => {
                let mut vars: Vars = HashMap::new();
                for (key, val) in self.vars {
                    if key != "solutions" && tags.contains(&key) {
                        vars.insert(key, val);
                    }
                }
                if tags.contains(&"solutions".to_string()) {
                    (vars, Some(self.solutions))
                } else {
                    (vars, None)
                }
            }
            Hide(tags) => {
                let mut vars: Vars = HashMap::new();
                for (key, val) in self.vars {
                    if key != "solutions" && !tags.contains(&key) {
                        vars.insert(key, val);
                    }
                }
                if !tags.contains(&"solutions".to_string()) {
                    (vars, Some(self.solutions))
                } else {
                    (vars, None)
                }
            }
        }
    }

    /// Checks if a problem contains all the variables in a template
    ///
    /// Returns None if the problem successfully passes the test, returns Some(MapmErr) otherwise
    ///
    /// # Usage
    ///
    /// ## Expected success
    ///
    /// ```
    /// use mapm::problem::Problem;
    /// use mapm::template::Template;
    /// use std::collections::HashMap;
    ///
    /// let problem = Problem {
    ///     name: String::from("problem"),
    ///     vars: HashMap::from([
    ///         (String::from("problem"), String::from("What is $1+1?$"))
    ///     ]),
    ///     solutions: vec![HashMap::from([
    ///         (String::from("text"), String::from("Some say the answer is $2$.")),
    ///         (String::from("author"), String::from("Dennis Chen")),
    ///     ])]
    /// };
    ///
    /// let template = Template {
    ///     name: String::from("template"),
    ///     engine: String::from("pdflatex"),
    ///     problem_count: Some(1),
    ///     texfiles: HashMap::from([
    ///         (String::from("problems.tex"), String::from("problems.pdf"))
    ///     ]),
    ///     vars: vec![String::from("title"), String::from("year")],
    ///     problemvars: vec![String::from("problem")],
    ///     solutionvars: vec![String::from("text"), String::from("author")],
    /// };
    ///
    /// assert!(problem.check_template(&template).is_none());
    /// ```
    ///
    /// ## Expected failure
    ///
    /// ```
    /// use mapm::problem::Problem;
    /// use mapm::template::Template;
    /// use mapm::result::MapmErr;
    /// use mapm::result::MapmErr::*;
    /// use std::collections::HashMap;
    ///
    /// let problem = Problem {
    ///     name: String::from("problem"),
    ///     vars: HashMap::from([(String::from("problem"), String::from("What is $1+1?$"))]),
    ///     solutions: vec![HashMap::from([
    ///         (String::from("text"), String::from("Some say the answer is $2$.")),
    ///         (String::from("author"), String::from("Dennis Chen")),
    ///     ])]
    /// };
    ///
    /// let template = Template {
    ///     name: String::from("template"),
    ///     engine: String::from("pdflatex"),
    ///     problem_count: Some(1),
    ///     texfiles: HashMap::from([(
    ///         String::from("problems.tex"), String::from("problems.pdf")
    ///     )]),
    ///     vars: vec!(String::from("title"), String::from("year")) ,
    ///     problemvars: vec!(String::from("problem"), String::from("author")),
    ///     solutionvars: vec!(String::from("text"), String::from("author")),
    /// };
    /// let template_check = problem.check_template(&template).unwrap();
    ///
    /// assert_eq!(template_check.len(), 1);
    /// match &template_check[0] {
    ///     ProblemErr(err) => {
    ///         assert_eq!(err, "Does not contain key `author`");
    ///     }
    ///     _ => {
    ///         panic!("MapmErr type is not ProblemErr");
    ///     }
    /// }
    /// ```

    pub fn check_template(&self, template: &Template) -> Option<Vec<MapmErr>> {
        let mut mapm_errs: Vec<MapmErr> = Vec::new();

        for problemvar in &template.problemvars {
            if !self.vars.contains_key(problemvar) {
                mapm_errs.push(ProblemErr(format!("Does not contain key `{}`", problemvar)));
            }
        }
        let mut index: u32 = 0;
        for map in &self.solutions {
            index += 1;
            for solutionvar in &template.solutionvars {
                if !map.contains_key(solutionvar) {
                    mapm_errs.push(SolutionErr(format!(
                        "Solution `{}` does not contain key `{}`",
                        index, solutionvar,
                    )));
                }
            }
        }
        if !mapm_errs.is_empty() {
            Some(mapm_errs)
        } else {
            None
        }
    }
}

fn parse_problem_yaml(name: &str, yaml: &str) -> MapmResult<Problem> {
    match serde_yaml::from_str::<SerializedProblem>(yaml) {
        Ok(problem) => {
            let mut vars: Vars = HashMap::new();
            for (key, val) in problem.vars {
                match val {
                    Value::String(val) => {
                        vars.insert(key, val);
                    }
                    Value::Number(val) => {
                        vars.insert(key, val.to_string());
                    }
                    Value::Bool(val) => {
                        match val {
                            true => vars.insert(key, String::from("true")),
                            false => vars.insert(key, String::from("false")),
                        };
                    }
                    _ => {
                        return Err(ProblemErr(format!("Could not parse key `{}`", key)));
                    }
                }
            }

            let solutions: Solutions = match problem.solutions {
                Some(solutions) => solutions,
                None => Vec::new(),
            };

            Ok(Problem {
                name: String::from(name),
                vars,
                solutions,
            })
        }
        Err(err) => Err(ProblemErr(err.to_string())),
    }
}

/// Converts a vector of problems into TeX files and writes them in the current working directory
///
/// Internal function used for compiling vectors of problems and for compiling contests
///
/// Returns a header TeX string as well (for contest compilation if necessary)

pub(crate) fn write_as_tex(problem_vec: &Vec<Problem>) -> String {
    let mut problem_number = 0;
    let mut headers = String::new();

    headers.push_str(&format!(
        "\\expandafter\\def\\csname mapm@probcount\\endcsname{{{}}}",
        problem_vec.len()
    ));

    for problem in problem_vec {
        problem_number += 1;
        headers.push_str(&format!(
            "\\expandafter\\def\\csname mapm@solcount@{}\\endcsname{{{}}}",
            problem_number,
            problem.solutions.len()
        ));
        headers.push_str(&format!(
            "\\expandafter\\def\\csname mapm@probname@{}\\endcsname{{{}}}",
            problem_number, problem.name
        ));
        for (key, val) in &problem.vars {
            let filename = &format!("mapm-prob-{}-{}.tex", problem_number, key);
            fs::write(filename, val)
                .unwrap_or_else(|_| panic!("Could not write to `{}`", filename));
        }
        for (pos, map) in problem.solutions.iter().enumerate() {
            for (key, val) in map {
                let solution_number = pos + 1;
                let filename = &format!(
                    "mapm-sol-{}-{}-{}.tex",
                    problem_number, solution_number, key,
                );
                fs::write(filename, val)
                    .unwrap_or_else(|_| panic!("Could not write to `{}`", filename));
            }
        }
    }
    headers
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_parse() {
        use super::parse_problem_yaml;
        use std::collections::HashMap;

        let yaml = "problem: What is $1+1$?
author: Dennis Chen
solutions:
  - text: It's probably $2$.
    author: Dennis Chen
  - text: The answer is $2$, but my proof is too small to fit into the margin.
    author: Pierre de Fermat
";
        let problem = parse_problem_yaml("problem", yaml).unwrap();

        let mut vars = HashMap::new();
        vars.insert(String::from("problem"), String::from("What is $1+1$?"));
        vars.insert(String::from("author"), String::from("Dennis Chen"));

        let mut solution_one = HashMap::new();
        solution_one.insert(String::from("text"), String::from("It's probably $2$."));
        solution_one.insert(String::from("author"), String::from("Dennis Chen"));
        let mut solution_two = HashMap::new();
        solution_two.insert(
            String::from("text"),
            String::from("The answer is $2$, but my proof is too small to fit into the margin."),
        );
        solution_two.insert(String::from("author"), String::from("Pierre de Fermat"));

        let solutions = vec![solution_one, solution_two];

        assert_eq!(problem.name, "problem");
        assert_eq!(problem.vars, vars);
        assert_eq!(problem.solutions, solutions);
    }
}
