//! Function for fetching templates

use std::collections::HashMap;

use crate::result::MapmErr::*;
use crate::result::MapmResult;

use std::fs;
use std::path::Path;

use serde::{Deserialize, Serialize};

pub struct Template {
    pub name: String,
    pub engine: String,
    pub texfiles: HashMap<String, String>,
    pub problem_count: Option<u32>,
    pub vars: Vec<String>,
    pub problemvars: Vec<String>,
    pub solutionvars: Vec<String>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
struct SerializedTemplate {
    engine: String,
    texfiles: HashMap<String, String>,
    problem_count: Option<u32>,
    vars: Vec<String>,
    problemvars: Vec<String>,
    solutionvars: Vec<String>,
}

/// Gets a template config with the given name from the passed in directory 

pub fn fetch_template_config<T: AsRef<Path>>(
    template_name: &str,
    templates_dir: T,
) -> MapmResult<Template> {
    let templates_dir: &Path = templates_dir.as_ref();
    let template_path = &templates_dir.join(template_name).join("config.yml");
    match fs::read_to_string(template_path) {
        Ok(template_yaml) => parse_template_yaml(template_name, &template_yaml),
        Err(_) => Err(TemplateErr(format!(
            "Could not read template `{}` from {:?}",
            template_name, template_path,
        ))),
    }
}

fn parse_template_yaml(name: &str, yaml: &str) -> MapmResult<Template> {
    match serde_yaml::from_str::<SerializedTemplate>(yaml) {
        Ok(template) => Ok(Template {
            name: String::from(name),
            engine: template.engine,
            texfiles: template.texfiles,
            problem_count: template.problem_count,
            vars: template.vars,
            problemvars: template.problemvars,
            solutionvars: template.solutionvars,
        }),
        Err(err) => Err(TemplateErr(err.to_string())),
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_parse() {
        use super::parse_template_yaml;
        use std::collections::HashMap;

        let template_yaml = r#"engine: pdflatex
texfiles:
  problems.tex: "${title}.PDF"
  solutions.tex: "${title}-sols.PDF"
problem_count: 1
vars:
  - title
  - year
problemvars:
  - problem
solutionvars:
  - text
  - author
"#;

        let template = parse_template_yaml("template", template_yaml).unwrap();

        let mut texfiles = HashMap::new();
        texfiles.insert(String::from("problems.tex"), String::from("${title}.PDF"));
        texfiles.insert(
            String::from("solutions.tex"),
            String::from("${title}-sols.PDF"),
        );

        assert_eq!(template.name, "template");
        assert_eq!(template.engine, "pdflatex");
        assert_eq!(template.texfiles, texfiles);
        assert_eq!(
            template.vars,
            vec![String::from("title"), String::from("year")]
        );
        assert_eq!(template.problemvars, vec![String::from("problem")]);
        assert_eq!(
            template.solutionvars,
            vec![String::from("text"), String::from("author")]
        );
    }
}
